#! /usr/bin/python
__author__ = "mike"
__date__ = "$27/08/2019 11:28:39 AM$"

from Clases.Alumno import Alumno
from ordenamientos import AlgoritmosOrdenamiento as ao

''' Implementacion del algoritmo de busqueda lineal O(n).'''    
def busquedaSecuencialI(lista, x):    
    #recorremos linealmente la lista
    for indice in range(len(lista)):
        if(lista[indice] == x):
            return True
    #si no econtramos el valor buscado, devolvemos -1
    return False 

#Busqueda secuencial (lineal) recursiva O(n)
def busquedaSecuencialR(lista, dato, indice):
    #caso base
    if len(lista) == 0 or indice == len(lista):
        return False
    #caso base
    if lista[indice] == dato:
        return True
    #llamada recursiva
    else:
        return busquedaSecuencialR(lista, dato, indice+1)
    
#Busqueda binaria iterativa O(log n)   
def busqueda_BinariaI(lista, numero):
    izq, der = 0, len(lista) - 1
    while izq <= der:
        mitad = izq + (der - izq) // 2
        numero_mitad = lista[mitad]
        if numero_mitad == numero:
            return True
        elif numero_mitad < numero:
            izq = mitad + 1
        else:
            der = mitad - 1
    return False

#Busqueda binaria recursiva, se asume que los datos estan ordenados O(logn)
def busquedaBinariaR(lista, dato, inicio, final):
    #caso base
    if final < inicio:
        return False
    #buscamos a la mitad
    mitad = (inicio + final) // 2
    #caso base
    if lista[mitad] == dato:
        return True
    if dato < lista[mitad]:
        #llamada recursiva
        return busquedaBinariaR(lista, dato, inicio, mitad-1)
    else:
        #llamada recursiva
        return busquedaBinariaR(lista, dato, mitad + 1, final)
    
#Pruebas de las buquedas
def main():
    lista = [3,6,9,3,6,29,55]
    print busquedaSecuencialR(lista, 3, 0)
    lista2 = [1,4,6,8,9,11,45,67,100]
    print busquedaBinariaR(lista2, 1, 0, len(lista2))
    
    #pruebas con Alumnos
    al1 = Alumno('Mike', 2245, 29)
    al2 = Alumno('Hugo', 111, 27)
    al3 = Alumno('Aitor', 222, 28)
    al4 = Alumno('Pedro', 333, 31)
    
    al5 = Alumno('Edgar', 444, 31)
    
    alumnos = [al1, al2, al3, al4]
    
    #se busca de manera secuencial un alumno que si pertenece a la lista de alumnos
    print busquedaSecuencialR(alumnos, al4, 0)
    
    #se busca un alumno que no pertenece a la lista de alumnos
    print busquedaSecuencialR(alumnos, al5, 0)
    
    #para poder hacer uso de la busqueda binaria es necesario ordenar los datos
    #como bubble es 'in situ', trabaja sobre la lista de alumnos
    ao.bubbleSort(alumnos)
    
    #se imprime la lista para asegurarnos que esta ordenada
    for alumno in alumnos:
        print alumno
    
    #buscamos un alumno que si pertenece a la lista
    print busquedaBinariaR(alumnos, al4, 0, len(alumnos))

    #buscamos un alumno que no pertenece a la lista
    print busquedaBinariaR(alumnos, al5, 0, len(alumnos))
    
if __name__ == "__main__":
    main()
