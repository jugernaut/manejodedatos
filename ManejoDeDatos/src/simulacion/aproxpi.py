#! /usr/bin/python

__author__ = "mike"
__date__ = "$24/05/2019 05:07:14 PM$"

import matplotlib.pyplot as plt
import numpy as np

def mc_pi_aprox(N=10000):
    #tamano de la imagen
    plt.figure(figsize=(7,5))
    #se generan valores de x,y de manera uniforme
    x, y = np.random.uniform(-1, 1, size=(2, N))
    #interior contiene las corrdenadas de los
    #dardos que calleron en la circunferencia
    interior = (x**2 + y**2) <= 1
    #se calcula el valor aproximado de pi
    pi = interior.sum() * 4 / float(N)
    #se calcula el error abs
    error = abs((pi - np.pi) / pi) * 100
    #dardos que calleron fuera
    exterior = np.invert(interior)
    #seccion de graficas
    plt.plot(x[interior], y[interior], 'b.')
    plt.plot(x[exterior], y[exterior], 'r.')
    plt.plot(0, 0, label='$\hat \pi$ = {:4.4f}\nerror = {:4.4f}%'
             .format(pi,error), alpha=0)
    plt.legend(frameon=True, framealpha=0.9, fontsize=16)
    plt.show()

mc_pi_aprox()
