#! /usr/bin/python

__author__ = "mike"
__date__ = "$27/05/2019 04:00:43 PM$"

import numpy as np

# Integral de montecarlo definida entre [0,1]
def integralMonteCarlo(f, N):
    puntos = np.random.uniform(size=N)
    total=0
    for i in range(N):
        total+=f(puntos[i])
    aprox = total/N
    return aprox

# Integral de montecarlo en un intervalo [a,b]
def integralMonteCarloG(f, N, a, b):
    puntos = np.random.uniform(size=N)
    total=0
    for i in range(N):
        total+=f(puntos[i]*(b-a)+a)
    aprox = (b-a)*total/N
    return aprox

# Integral triple de montercarlo en [0,1]
def integralMonteCarloTriple(f, N):
    x = np.random.uniform(size=N)
    y = np.random.uniform(size=N)
    z = np.random.uniform(size=N)
    total = 0
    for i in range(N):
        total+=f(x[i], y[i], z[i])
    aprox = total/N
    return aprox

# funcion a integrar
def f(x):
    return x**2

# funcion a integrar
def f3(x, y, z):
    return 2*x + y - z**2

def main():
    # Pruebas de aproximaciones por MonteCarlo
    print integralMonteCarlo(f, 100000)
    
    print integralMonteCarloG(f, 100000, 1.0, 2.0)
    
    print integralMonteCarloTriple(f3, 100000)

main()
