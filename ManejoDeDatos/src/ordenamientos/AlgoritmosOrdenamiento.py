#! /usr/bin/python
__author__ = "mike"
__date__ = "$14/08/2019 05:02:35 PM$"
from Clases.Alumno import Alumno


#Ordenamiento por insercion O(n*n)
def insertionSort(lista):
    #asigno el largo de la lista a una variable
    n=len(lista)
    for i in range(1,n):
        #por cada elemento que esta antes del elemento que se esta tomando en cuenta,
        for j in range(0,i):
            #si algun elemento anterior resulta mayor que el elemento que se esta tomando en cuenta
            if lista[j]>lista[i]: 
                lista[j], lista[i] = lista[i], lista[j]
    #devuelve la lista para que pueda ser usada.
    return lista

#Ordenamiento por sleccion O(n*n)
def selectionSort(lista):
    #asigno el largo de la lista a una variable
    n = len(lista)
    #para cada elemento de la lista
    for i in range(n):
        #suponemos que tenemos al minimo
        minimo = i 
        #comparamos con el resto de los elementos
        for j in range(i+1, n): 
            #si se encuentra un minimo se actualiza el indice
            if lista[minimo] > lista[j]: 
                minimo = j 
        #se realiza el cambio con el minimo
        lista[i], lista[minimo] = lista[minimo], lista[i] 
    return lista

#Ordenamiento burbuja O(n*n)
def bubbleSort(lista):
    #asigno el largo de la lista a una variable
    n = len(lista)
    #bandera para identificar cambios
    intercambio = True
    #variable para saber cuantos han sido ordenados
    j = 0
    #si hay cambios entro al ciclo
    while(intercambio):
        #los j-esimos elementos ya han sido ordenados
        j = j+1
        #asumimos que no hay intercambios
        intercambio = False
        #los ultimos 
        for i in range(0, n-j):
            if lista[i] > lista[i+1] :
                lista[i], lista[i+1] = lista[i+1], lista[i]
                intercambio = True
    #devuelve la lista
    return lista

#Ordenamiento por mezcla O(n*log n)
def mergeSort(lista):
    if (len(lista) < 2):
        return lista
    else:
        medio = len(lista)/2
        izquierda = mergeSort(lista[:medio])
        derecha = mergeSort(lista[medio:])
        return merge(izquierda, derecha)

#funcion auxiliar para mezclar 2 listas O(n)
def merge(l1, l2):
    i,j=0,0
    mezcla=[]
    while(i < len(l1) and j < len(l2)):
        if l1[i] <= l2[j]:
            mezcla.append(l1[i])
            i = i+1
        else:
            mezcla.append(l2[j])
            j = j+1  
    # Agregar lo que falta, si i o j ya son len(lista) no agrega nada.
    mezcla += l1[i:]
    mezcla += l2[j:]

    return mezcla

#Ordenamiento rapido O(n*log n)
def quickSort(lista):
    if len(lista) < 2:
        return lista
    else:
        menores, pivote, mayores = particion(lista)
        return quickSort(menores)+[pivote]+quickSort(mayores)

#funcion auxiliar que parte una lista en mayores, menores y pivote O(n)
def particion(lista):
    mayores, menores = [], []
    pivote = lista[0]
    for i in range(1,len(lista)):
        if lista[i] < pivote:
            menores.append(lista[i])
        else:
            mayores.append(lista[i])
    return menores, pivote, mayores

def main():
    #pruebas con listas de numeros
    lista = [4,7,9,1,2,5]
    print lista
    print bubbleSort(lista)
    
    #pruebas con Alumnos
    al1 = Alumno('Mike', 2245, 29)
    al2 = Alumno('Hugo', 111, 27)
    al3 = Alumno('Aitor', 222, 28)
    al4 = Alumno('Pedro', 333, 31)
    
    alumnos = [al1, al2, al3, al4]
    #como bubble es 'in situ', trabaja sobre la lista de alumnos
    bubbleSort(alumnos)
    
    for alumno in alumnos:
        print alumno

if __name__ == "__main__":
    main()