#! /usr/bin/python

import numpy as np 
import matplotlib.pyplot as plt
import math

__author__ = "mike"
__date__ = "$27/02/2019 04:14:44 PM$"

def funcion(x):
    return x**2+1

def grafica(f):
    # creamos un espacio de 1000 puntos igualmente espaciados -10 and 10
    x = np.linspace(-10, 10, 1000)
    
    #se generan los valores a garficar
    fx = f(x)
    
    #se genera la grafica
    plt.plot(x, fx, 'b-', label='x**2 + 1')
    
    #se agrega la etiqueta para saber que se esta graficando
    plt.legend(frameon=True, framealpha=0.9, fontsize=16)
    
    #mostramos la grafica
    plt.show()

def main():
    grafica(funcion)

if __name__ == '__main__':
    main()