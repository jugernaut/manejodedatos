#! /usr/bin/python
__author__ = "mike"
__date__ = "$30/08/2019 12:42:18 PM$"
import AlgoritmosOrdenamiento as ao
import random
from time import time
import matplotlib.pyplot as plt
import numpy as np

'''Dado un algoritmo y una lista de listas de datos a ordenar, mide el tiempo
que le toma a un algoritmo ordenar cada lista'''
def mideTiempo(algoritmo, listadatos):
    tiempo = []
    for i in range(len(listadatos)):
        tiempoi = time()
        algoritmo(listadatos[i])
        tiempof = time()
        tiempo.append(tiempof-tiempoi)
    return tiempo

#Genera los tiempos necesarios para graficar
def generaTiempos():
    #se genera una lista de tam 100 y 2 copias identicas
    lista100I = random.sample(range(1000),100)
    lista100S = lista100I[:]
    lista100B = lista100I[:]
    #se genera una lista de tam 1000 y 2 copias identicas
    lista1000I = random.sample(range(10000),1000)
    lista1000S = lista1000I[:]
    lista1000B = lista1000I[:]
    #se genera una lista de tam 10000 y 2 copias identicas
    lista10000I = random.sample(range(100000),10000)
    lista10000S = lista10000I[:]
    lista10000B = lista10000I[:]
    
    datosI = [lista100I, lista1000I, lista10000I]
    datosS = [lista100S, lista1000S, lista10000S]
    datosB = [lista100B, lista1000B, lista10000B]
    #calculamos los tiempos que le toma a cada algoritmo ordenar las listas
    tiemposI = mideTiempo(ao.insertionSort, datosI)
    tiemposS = mideTiempo(ao.selectionSort, datosS)
    tiemposB = mideTiempo(ao.bubbleSort, datosB)
    
    return tiemposI, tiemposS, tiemposB

'''Funcion que genera la grafica de numero de datos v.s. tiempos que
le toma a cada algoritmo ordenar el mismo conjunto de datos'''
def generaGrafica(datos, tiempos):
    plt.plot(datos, tiempos[0], 'r-', label='selection', markevery=10000)
    plt.plot(datos, tiempos[1], 'b-', label='insertion', markevery=10000)
    plt.plot(datos, tiempos[2], 'g-', label='bubble', markevery=10000)
    plt.legend(frameon=True, framealpha=0.9, fontsize=16)
    
    plt.show()
    
def main():
    datos = [100, 1000, 10000]
    inser, selec, bubble = generaTiempos()
    generaGrafica(datos, [inser, selec, bubble])
    
if __name__ == "__main__":
    main()
