#! /usr/bin/python
# -*- coding: utf-8 -*-
__author__ = "mike"
__date__ = "$4/09/2019 03:51:51 PM$"

from arboles.ArbolBinario import Nodo
from string import lower

'''Clase basada en arboles binarios para identificar en que
animal estas pensando'''
class ArbolAdivinador(object):
    
    # Constructor de adivinadores
    def __init__(self):
        self.raiz = Nodo("pajaro")
        
    #Funcion para determinar si se desea continuar con mas preguntas
    def si(self, preg):
        #se muestra una pregunta en consola y se pide la respuesta del usuario
        resp = lower(raw_input(preg))
        return (resp[0] == 's')
    
    def adivina(self):
        bucle = True
        while bucle:
            #en caso de no querer continuar con mas preguntas
            if not self.si("Estas pensando en un animal? "):
                print 'Gracias por jugar :)'
                break
            #nodo temporal que sirve para recorrer el arbol
            temporal = self.raiz
            #mientras haya hijo izquierdo se pueden hacer preguntas
            while temporal.izq != None:
                #en caso afirmativo avanza al hijo izquierdo
                if self.si(temporal.dato + "? "):
                    temporal = temporal.izq
                #de otra forma avanza al hijo derecho
                else:
                    temporal = temporal.der
            #se llega a un nodo hoja
            animal = temporal.dato
            #si se adivino termina, en caso contrario hay que obtener info.
            if self.si("Es un " + animal + "? "):
                print "¡Soy el más grande!"
                print "███████████████████████████████─"
                print "──────▀█▄▀▄▀████▀──▀█▄▀▄▀████▀──"
                print "────────▀█▄█▄█▀──────▀█▄█▄█▀────"
                continue
            #nuevo animal
            nuevo = raw_input("¿Qué animal era? ")
            #nueva pregunta para diferenciar a la hoja actual del nuevo animal
            info = raw_input("¿Qué diferencia a un " + animal + " de un " + nuevo + "? ")
            #se actualiza el nodo con la nueva pregunta
            temporal.dato = info
            #se agrega como izquierdo al animal que estaba en la hoja
            temporal.izq = Nodo(animal)
            #y como derecho el nuevo animal
            temporal.der = Nodo(nuevo)
            
def main():
    adivino = ArbolAdivinador()
    adivino.adivina()

if __name__ == "__main__":
    main()
    
