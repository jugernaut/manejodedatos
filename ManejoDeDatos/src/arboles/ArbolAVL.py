# -*- coding: utf-8 -*-
#! /usr/bin/python
'''
Created on 29/03/2017

@author: mike
'''
from ArbolBinarioBusqueda import ABB

''' Clase ArbolAVL, genera la estructura de un arbol AVL, asi como sus funciones.
    Dado que hereda de la clase ABB, la clase AVL cuenta con todas las funciones
    de la clase ABB'''
class ArbolAVL(ABB):

    def __init__(self, dato):
        super(ArbolAVL, self).__init__(dato)
        
    #Devuelve el ultimo nodo agregado para balanceo en caso necesario
    def ultimo(self):
        return self.ultimo_agregado

    #Determina si un nodo esta balanceado
    def balanceado(self, nodo):
        return abs(self.altura(nodo.izq)-self.altura(nodo.der)) <= 1

    #Inserta un nodo en el arbol y lo balancea, si es que se desbalanceo
    def insertar(self, dato):
        #Se inserta el dato usando el metodo de la clase padre
        super(ArbolAVL, self).insertar(dato)
        #Identificamos donde se inserto el dato
        ultimo_nodo_insertado = self.ultimo()
        
        #Se rebalancean los nodos que hayan quedado desbalanceados
        #Y se empieza desde el padre, por que nodo por def, no pudo
        #haber quedado desbalanceado
        self.balancea(ultimo_nodo_insertado.padre)
        
    #Borra un nodo en el arbol y lo balancea, si es que se desbalanceo
    def borra(self, dato):
        #Se borra el nodo usando el metodo de la clase padre
        borradopadre = self.borra_nodo(self.raiz, dato)
        
        #Se rebalancean los nodos que hayan quedado desbalanceados
        #Y se empieza desde el padre, por que nodo por def, no pudo
        #haber quedado desbalanceado
        self.balancea(borradopadre)
    
    #Balancea el arbol, a partir de un nodo
    def balancea(self, nodo):
        #caso base
        if nodo is None:
            return
        if not self.balanceado(nodo):
            factorNodo = self.factor_equilibrio(nodo)
            factorIz = self.factor_equilibrio(nodo.izq)
            factorDer = self.factor_equilibrio(nodo.der)
            #Si el nodo esta desbalanceado se tendra uno de 4 casos
            #mismos que se solucionan con alguna de las 4 rotaciones
            if factorNodo == -2 and (factorDer == -1 or factorDer == 0): #probado
                self.rotacionSimpleIzquierda(nodo)
            if factorNodo == 2 and (factorIz == 1 or factorIz == 0): #probado
                self.rotacionSimpleDerecha(nodo)
            if factorNodo == -2 and factorDer == 1: #probado bien
                self.rotacionDobleIzquierda(nodo)
            if factorNodo == 2 and factorIz == -1: #probado bien
                self.rotacionDobleDerecha(nodo) 
        #llamada recursiva para balancear toda la ruta. Desde donde se inserto
        #el nodo, hasta la raiz        
        self.balancea(nodo.padre)        

    '''
    2 = el subarbol esta muy cargado a la izquierda
    1 = el subarbol esta cargado a la izquierda
    0 = no hace falta rotar
    -1 = el subarbol esta cargado a la derecha
    -2 = el subarbol esta muy cargado a la derecha
    '''
    def factor_equilibrio(self, nodo):
        if nodo is None:
            return 0
        else:
            return self.altura(nodo.izq)-self.altura(nodo.der)
    
    #Rota el nodo a la derecha
    def rotacionSimpleDerecha(self, nodo):
        if nodo is not None:
            if nodo is self.raiz:
                #actualizamos la raiz
                self.raiz = nodo.izq
                self.raiz.padre = None
                # si el nodo.izq tiene hijo derecho se pasa como izq de nodo
                if nodo.izq.der is not None:
                    nodo.izq.der.padre = nodo
                    nodo.izq = nodo.izq.der
                else:
                    nodo.izq = None
                self.raiz.der = nodo
                nodo.padre = self.raiz
            else:
                nuevaraiz = nodo.izq
                #actualizamos la nueva raiz
                if nodo.esIzq():
                    nodo.padre.izq = nuevaraiz
                else:
                    nodo.padre.der = nuevaraiz
                nuevaraiz.padre = nodo.padre
                # si el nodo.izq tiene hijo derecho se pasa como izq de nodo
                if nuevaraiz.der is not None:
                    nuevaraiz.der.padre = nodo
                    nodo.izq = nuevaraiz.der
                else:
                    nodo.izq = None
                nuevaraiz.der = nodo
                nodo.padre = nuevaraiz
      
    #Rota el nodo a la izquierda            
    def rotacionSimpleIzquierda(self, nodo):
        if nodo is not None:
            if nodo is self.raiz:
                #actualizamos la raiz
                self.raiz = nodo.der
                self.raiz.padre = None
                # si el nodo.der tiene hijo izquierdo se pasa como der de nodo
                if nodo.der.izq is not None:
                    nodo.der.izq.padre = nodo
                    nodo.der = nodo.der.izq
                else:
                    nodo.der = None
                self.raiz.izq = nodo
                nodo.padre = self.raiz
            else:
                nuevaraiz = nodo.der
                #actualizamos la nueva raiz
                if nodo.esIzq():
                    nodo.padre.izq = nuevaraiz
                else:
                    nodo.padre.der = nuevaraiz
                nuevaraiz.padre = nodo.padre
                # si el nodo.der tiene hijo izquierdo se pasa como der de nodo
                if nuevaraiz.izq is not None:
                    nuevaraiz.izq.padre = nodo
                    nodo.der = nuevaraiz.izq
                else:
                    nodo.der = None
                nuevaraiz.izq = nodo
                nodo.padre = nuevaraiz
                
    #Define la doble rotacion a la derecha, a partir de un nodo            
    def rotacionDobleDerecha(self, nodo):
        self.rotacionSimpleIzquierda(nodo.izq)
        self.rotacionSimpleDerecha(nodo)
    
    #Define la doble rotacion a la izquierda, a partir de un nodo     
    def rotacionDobleIzquierda(self, nodo):
        self.rotacionSimpleDerecha(nodo.der)
        self.rotacionSimpleIzquierda(nodo)
        
def main():
    aavl = ArbolAVL(15)
    aavl.insertar(10)
    aavl.insertar(20)
    aavl.insertar(8)
    aavl.insertar(14)
    aavl.insertar(0)
    aavl.insertar(35)
    aavl.insertar(99)
    aavl.insertar(30)
    
    aavl.borra(10)
    aavl.borra(0)
    
    print 'recorido en orden'
    aavl.recorrido_enorden(aavl.raiz)
    print 'recorido en preorden'
    aavl.recorrido_preorden(aavl.raiz)
    print 'recorido en postorden'
    aavl.recorrido_postorden(aavl.raiz)
    
main()
                    