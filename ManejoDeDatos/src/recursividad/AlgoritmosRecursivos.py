#! /usr/bin/python
__author__ = "mike"
__date__ = "$19/08/2019 12:47:38 PM$"

def factorialR(n):
    #Caso base
    if n < 2:
        return 1
    #Llamada recursiva
    else: 
        return n * factorialR(n-1)
    
def factorialI(n):
    if n < 2:  
        return 1
    else:
        fact = n
        for num in range(2,n):
            fact = fact * num
        return fact
    
def fibonacciR(n):
    #Casos base
    if n == 1:
        return 0
    if n == 2:
        return 1
    #Llamadas recursivas
    else:
        return fibonacciR(n-2) + fibonacciR(n-1)
    
def fibonacciI(n):
    fibo1, fibo2 = 0, 1
    for i in range(1, n-1):
        fibo1, fibo2 = fibo2, fibo1 + fibo2
    return fibo2


def torresHanoi(ndiscos, pilaA, pilaB, pilaC):
    #Caso base
    if ndiscos == 1:
        print 'mover disco: '+str(ndiscos)+' de' + pilaA + ' a ' + pilaC
    #Llamadas recursivas
    else:
        torresHanoi(ndiscos-1, pilaA, pilaC, pilaB)
        print 'mover disco: '+str(ndiscos)+ ' de ' + pilaA + ' a ' + pilaC
        torresHanoi(ndiscos-1, pilaB, pilaA, pilaC)


if __name__ == "__main__":
    #Prueba de factorial
    print factorialR(4)
    
    #Prueba de factorial
    print factorialI(4)
    
    #Prueba fibonacci
    print fibonacciR(4)
    
    #Prueba fibonacci
    print fibonacciI(4)
    
    #Prueba Hanoi
    #torresHanoi(3, 'pilaA', 'pilaB', 'pilaC')
