#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = "miriam.padilla"
__date__ = "$24/09/2019 01:45:28 PM$"
import re

'''Las expresiones regulares, también conocidas como regex, 
son patrones de búsqueda definidos con una sintaxis formal.

Siempre que sigamos sus reglas, podremos realizar búsquedas simples 
y avanzadas, que utilizadas en conjunto con otras funcionalidades, 
las vuelven una de las opciones más útiles e importantes 
referencias http://w3.unpocodetodo.info/utiles/regex.php'''

def main():
    texto = "En esta cadena se encuentra la palabra que buscamos "

    palabra = "palabra"
    palabra2 = "Hola"

    encontrado = re.search(palabra,  texto)
    print encontrado
    
    encontrado2 = re.search(palabra2,  texto)
    print encontrado2

    if encontrado:
        print "Se ha encontrado la palabra:", palabra
    else:
        print "No se ha encontrado la palabra:", palabra

    if encontrado2:
        print "Se ha encontrado la palabra:", palabra
    else:
        print "No se ha encontrado la palabra:", palabra
    # Posición donde empieza la coincidencia
    print( encontrado.start() ) 
    # Posición donde termina la coincidencia
    print( encontrado.end() )  
    # Tupla con posiciones donde empieza y termina la coincidencia
    print( encontrado.span() )   
    # Cadena sobre la que se ha realizado la búsqueda
    print( encontrado.string )
    
    
    #Sigamos 
 	#re.split: divide una cadena a partir de un patrón:
    texto = "Vamos a dividir esta cadena"

    res = re.split(' ', texto)
    print res
    
    #re.sub: sustituye todas las coincidencias en una cadena:
    texto = "Hola amigo"

    res = re.sub('amigo', 'amiga', texto)
    print res
    #re.findall: busca todas las coincidencias en una cadena:
    texto = "hola adios hola hola"

    res = re.findall('hola', texto)
    print res
    #Aquí se nos devuelve una lista, pero podríamos aplicar la función len() para saber el número:
    total = len(re.findall('hola', texto))
    print total
    
    #############Patrones con varios valores###############

    #Si queremos comprobar varias posibilidades, podemos utilizar un | a modo de OR.
    #Generalmente pondremos el listado de alternativas entre paréntesis ():
    texto = "hola adios hello bye"

    res = re.findall('hola|hello', texto)
    print res
    #Aquí se nos devuelve una lista, pero podríamos aplicar la función len() para saber el número:
    total = len(re.findall('hola|hello', texto))
    print total
    
#Patrones con sintaxis repetida 
# vamos a crear una función capaz de ejecutar varios patrones en una lista sobre un texto:
def buscar(patrones, texto):
    for patron in patrones:
        print( re.findall(patron, texto) )
        
def main2():
    texto = "hla hola hoola hooola hooooola"
    patrones = ['hla', 'hola', 'hoola']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscador = buscar(patrones, texto)

    ######META CARACTERES##### 
    #son símbolos que tienen un significado especial cuando se colocan dentro de una expresión regular
    #Hay muchos metacaracteres

    # * indica cero o más coincidencias del carácter que viene immediatamente
    
    patrones = ['ho','ho*','ho*la','hu*la']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscador = buscar(patrones, texto)
    
    # + funciona de forma similar al anterior, pero indica por lo menos una coincidencia
    
    patrones = ['ho*', 'ho+']  
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones

    buscar(patrones, texto)
    
    # ? indica como máximo una coincidencia del carácter que viene inmediatamente antes.
    
    patrones = ['ho*', 'ho+', 'ho?', 'ho?la']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones

    buscar(patrones, texto)
    
    # {n} Lo utilizaremos para definir 'n' repeticiones exactas de la letra a la izquierda del meta-carácter:
    
    patrones = ['ho{0}la', 'ho{1}la', 'ho{2}la']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones

    buscar(patrones, texto)
    
    #{n, m} Lo utilizaremos para definir un número de repeticiones variable entre 'n' y 'm' de la letra a la izquierda
    #del meta-carácter
    
    patrones = ['ho{0,1}la', 'ho{1,2}la', 'ho{2,9}la']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones

    buscar(patrones, texto)
    
    #Cuando nos interese crear un patrón con distintos carácteres, podemos definir conjuntos entre paréntesis
    
    texto = "hala hela hila hola hula"

    patrones = ['h[ou]la', 'h[aio]la', 'h[aeiou]la']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscar(patrones, texto)
    
    # Podemos utilizar con repeticiones
    
    texto = "haala heeela hiiiila hoooooola"

    patrones = ['h[ae]la', 'h[ae]*la', 'h[io]{3,9}la']
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscar(patrones, texto)
    
    
def main3():
    ####Exclusión en grupos ^ ###########

    texto = "hala hela hila hola hula"

    patrones = ['h[o]la', 'h[^o]la'] 
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscar(patrones, texto)
    # Rangos - 
    #[A-Z]: Cualquier caracter alfabetico en mayuscula (no especial ni numero).
    #[a-z]: Cualquier caracter alfabetico en minuscula (no especial ni numero).
    #[A-Za-z]: Cualquier caracter alfabetico en minuscula o mayuscula (no especial ni numero).
    #[A-z]: Cualquier caracter alfabetico en minuscula o mayuscula (no especial ni numero).
    #[0-9]: Cualquier caracter numerico (no especial ni alfabetico).
    #[a-zA-Z0-9]: Cualquier caracter alfanumerico (no especial).

    ##cualquier rango puede ser excluido para conseguir el patron contrario.

    texto = "hola h0la Hola mola m0la M0la"

    patrones = ['h[a-z]la', 'h[0-9]la', '[A-z]{4}', '[A-Z][A-z0-9]{3}'] 
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscar(patrones, texto)

def main4():
    #Si cada vez que quisieramos definir un patron variable tuvieramos que crear rangos, 
    #al final tendriamos expresiones regulares gigantes. Por suerte su sintaxis tambien 
    #acepta una serie de caracteres escapados que tienen un significo unico. Algunos de los mas importantes son:
    #Codigo	Significado
    #\d	numerico
    #\D	no numerico
    #\s	espacio en blanco
    #\S	no espacio en blanco
    #\w	alfanumerico
    #\W	no alfanumerico

    #las cadenas no tienen en cuenta el \ a no ser que especifiquemos que son cadenas en crudo (raw), 
    #por lo que tendremos que precedir las expresiones regulares con una 'r
    texto = "Este es un curso de Python en el anio 2019"

    patrones = [r'\d+', r'\D+', r'\s+', r'\S+', r'\w+', r'\W+'] 
    
    print "Del Texto  ", texto , "  Buscaremos ", patrones
    
    buscar(patrones, texto) 
        
if __name__ == "__main__": 
    #main()
    #main2()
    #main3()
    main4()


