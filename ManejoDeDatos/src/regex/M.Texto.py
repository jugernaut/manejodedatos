#!/usr/bin/env python
# -*- coding: utf-8 -*-


# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "miriam.padilla"
__date__ = "$26/09/2019 01:35:19 PM$"

def main():
#Abrimos el archivo  usando la ruta en donde se encuentra el mismo
    texto = open('../../utils/ManejodeDatos9180.txt', 'r')
#Leemos el archivo
    print texto.read()
    
def main2():
#Importamos la paqueteria de Expresiones Regulares
    import re
    
    #Cargamos el archivo .txt
    texto = open('../../utils/ManejodeDatos9180.txt')
    
#Recorremos cada una de las linea y usando expresiones regulares obtenemos todas las cadenas que contengan @
#Usando la logica de que en el archivo, el correo estaba separado por un espacio antes y despues de la demas
#informacion
    a = 0
    for linea in texto:
        a=a+1
        linea = linea.rstrip()
        x = re.findall('\S+@\S+', linea)
        if len(x) > 0 :
            print a-1, x
    
if __name__ == "__main__":
    main()
    main2()

