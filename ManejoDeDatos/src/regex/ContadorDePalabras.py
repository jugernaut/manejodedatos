#! /usr/bin/python
# -*- coding: utf-8 -*-
__author__ = "mike"
__date__ = "$2/10/2019 12:52:57 PM$"
import re
from collections import Counter

'''Clase que ayuda a contar la frecuencia de las palabras
de algun archivo de texto que se recibe como parametro'''
class ContadorDePalabras():
    
    #Constructor que recibe la ruta del archivo que se va a leer
    def __init__(self, ruta):
        self.ruta = ruta
        self.diccionario = {}
        self.diccionarioOrdenado = ()
        
    #Aqui definir el resto de los metodos que se utilizan en el main

if __name__ == "__main__":
    wall_e = ContadorDePalabras("../../utils/inteligencia.txt")
    wall_e.machine_learning()
    wall_e.ordena_diccionario()
    wall_e.imprime_diccionario()
