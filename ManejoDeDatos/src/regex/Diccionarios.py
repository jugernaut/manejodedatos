#! /usr/bin/python
# -*- coding: utf-8 -*-
__author__ = "mike"
__date__ = "$25/09/2019 02:22:59 PM$"
import re
from collections import Counter

cadenota = '''Una factura factura de compra o factura comercial es un 
    documento mercantil que refleja toda la información de una operación de 
    compraventa La información fundamental que aparece en una factura debe 
    reflejar la entrega de un producto o la provisión de un servicio junto a 
    la fecha de devengo además de indicar la cantidad a pagar en relación a 
    existencias bienes de una empresa para su venta en eso ordinario de la 
    explotación o bien para su transformación o incorporación al proceso 
    productivo además de indicar el tipo de Impuesto sobre el Valor Añadido
    (IVA) que se debe aplicar'''
    
def machine_learning(ruta):
        # abrir el archivo con la ruta especificada
        # AQUI TU CODIGO
        
        # leer el archivo y guardarlo en la variable cadenota
        # AQUI TU CODIGO
    
        # generamos una lista con las palabaras utilizando regex
        lista_palabras = re.split('\s+', cadenota)
        
        # se crea un diccionario para contar la frecuencia de las palabras
        diccionario = {}

        # contamos la frecuencia de cada palabra
        
                # se agrega al diccionario las las veces que se a contado la palabra,
                # en caso de no existir dicha palabra se devuelve cero. En cualquier caso
                # se suma uno por cada vez que aparezca dicha palabra
    
        # (OPCIONAL)
        # usando el paquete collections podemos ordenar el diccionario, a pesar
        # de que no es una cualidad de los diccionaros
        contador = Counter(diccionario)
        diccionarioOrdenado = contador.most_common()
    
        # Se imprime el diccionario ordenado para saber facilmente de que trato el
        # el texto de la cadenota

if __name__ == "__main__":
    #machine_learning(ruta)
    ruta = "../../utils/inteligencia.txt"
    print ruta
    