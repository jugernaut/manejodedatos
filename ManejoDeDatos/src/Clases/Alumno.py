# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
class Alumno:
 
    def __init__(self, nombre, numero_cuenta, edad):
        self.nombre = nombre
        self.numero_cuenta = numero_cuenta
        self.edad = edad
 
    def __gt__(self, otro):
        return self.numero_cuenta > otro.numero_cuenta
 
    def __lt__(self, otro):
        return self.numero_cuenta < otro.numero_cuenta
    
    #Sobrecarga del operador de igualdad, para usarlo en las busquedas
    def __eq__(self, otro):
        return self.numero_cuenta == otro.numero_cuenta
 
    def __str__(self):
        return "Alumno con numero de cuenta " + str(self.numero_cuenta)